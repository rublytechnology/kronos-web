<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CKEditorController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NosotroController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ProyectoController;
use App\Http\Controllers\ServicioController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [PagesController::class, 'index'])->name('index');
Route::get('/nosotros', [PagesController::class, 'nosotros'])->name('nosotros');
Route::get('/servicios', [PagesController::class, 'servicios'])->name('servicios');
Route::get('/servicio/{id}', [PagesController::class, 'serviciosView'])->name('servicioView');

Route::get('/proyectos', [PagesController::class, 'proyectos'])->name('proyectos');
Route::get('/proyecto/{id}', [PagesController::class, 'proyectoView'])->name('proyectoView');
Route::get('/blog', [PagesController::class, 'blog'])->name('blog');
Route::get('/blog/{id}', [PagesController::class, 'blogView'])->name('blogView');

Route::get('/clientes', [PagesController::class, 'clientes'])->name('clientes');

Route::get('/contacto', function () {
    return view('contacto');
});
Route::get('/table', function () {
    return view('table');
});
Route::post('contactStore', [PagesController::class, 'contactStore'])->name('contactStore');
Route::post('ckeditor/image_upload', [CKEditorController::class, 'upload'])->name('upload');

Route::get('/clear', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    Artisan::call('key:generate');
    Artisan::call('storage:link');
    return "Cleared!";
});

Route::get('/migrateUpdate', function () {
    Artisan::call('migrate --path=database/migrations --seed');
    return "Cleared!";
});


Auth::routes(['register' => false]);
Route::get('logout',[LoginController::class, 'logout'])->name('logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/administracion', [AdminController::class, 'index'])->name('admin');
    Route::resource('admin-nosotros', NosotroController::class);
    Route::resource('admin-servicios', ServicioController::class);
    Route::resource('admin-clientes', ClienteController::class);
    Route::resource('admin-proyectos', ProyectoController::class);
    Route::resource('admin-blog', BlogController::class);
    Route::resource('admin-home', HomeController::class);
});