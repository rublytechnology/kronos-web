<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nosotro extends Model
{
    use HasFactory;
    protected $fillable = ['id','quienes_somos','img_quines','mision','img_mision','vision','img_vision'];

}
