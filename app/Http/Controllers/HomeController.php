<?php

namespace App\Http\Controllers;

use App\Models\ImgHome;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $imgs = ImgHome::get();
        return view('admin.imghome.index')->with('imgs',$imgs);
    }

    public function create()
    {
        return view('admin.imghome.create');
    }

    public function store(Request $request)
    {
        if($request->hasFile('img')){
            $name =  Storage::disk('public')->put('home',  $request->file('img'));
        }else{
            return redirect()->back()->with('error', 'La imagen es requerida para crear el cliente'); 
        }

        $servicio =  new ImgHome();
        $servicio->texto = $request->titulo;
        $servicio->img = $name;
        $servicio->save();
        return redirect()->back()->with('success', 'Se ha creado correctamente'); 
    }

    public function edit($id)
    {
        $servicio = ImgHome::find($id);
        return view('admin.imghome.edit')->with('proyecto',$servicio);
    }

    public function update(Request $request,$id)
    {
        $name = $request->hasFile('img') ? Storage::disk('public')->put('home',  $request->file('img')) :  $request->img_act;
        $servicio = ImgHome::find($id);
        $servicio->texto = $request->titulo;
        $servicio->img = $name;
        $servicio->save();
        return redirect()->back()->with('success', 'Se ha actualizado correctamente'); 
    }

    public function destroy($id)
    {
        $servicio = ImgHome::find($id)->delete();
        return redirect()->back()->with('success', 'Se ha eliminado correctamente'); 
    }
}
