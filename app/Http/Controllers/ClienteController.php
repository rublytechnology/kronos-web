<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Servicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::get();
        return view('admin.clientes.index')->with('clientes',$clientes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('img')){
            $name =  Storage::disk('public')->put('clientes',  $request->file('img'));
        }else{
            return redirect()->back()->with('error', 'La imagen es requerida para crear el cliente'); 
        }

        $servicio =  new Cliente();
        $servicio->titulo = $request->titulo;
        $servicio->descripcion = $request->descripcion;
        $servicio->img = $name;
        $servicio->save();

        return redirect()->back()->with('success', 'Se ha creado correctamente'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicio = Cliente::find($id);
        return view('admin.clientes.edit')->with('cliente',$servicio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->hasFile('img') ? Storage::disk('public')->put('servicios',  $request->file('img')) :  $request->img_act;
        $servicio = Cliente::find($id);
        $servicio->titulo = $request->titulo;
        $servicio->descripcion = $request->descripcion;
        $servicio->img = $name;
        $servicio->save();
        return redirect()->back()->with('success', 'Se ha actualizado correctamente'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $servicio = Cliente::find($id)->delete();
        return redirect()->back()->with('success', 'Se ha eliminado correctamente'); 
    }
}
