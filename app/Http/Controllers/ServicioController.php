<?php

namespace App\Http\Controllers;

use App\Models\Servicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ServicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicios = Servicio::get();
        return view('admin.servicios.index')->with('servicios',$servicios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.servicios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $name = $request->hasFile('img') ? Storage::disk('public')->put('home',  $request->file('img')) :  $request->img_act;

        if($request->hasFile('img')){
            $name =  Storage::disk('public')->put('servicios',  $request->file('img'));
        }else{
            return redirect()->back()->with('error', 'La imagen es requerida para crear el servicio'); 
        }

        $servicio =  new Servicio();
        $servicio->titulo = $request->titulo;
        $servicio->descripcion = $request->descripcion;
        $servicio->img = $name;
        $servicio->save();

        return redirect()->back()->with('success', 'Se ha creado correctamente'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Servicio  $servicio
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Servicio  $servicio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicio = Servicio::find($id);
        return view('admin.servicios.edit')->with('servicio',$servicio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Servicio  $servicio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $name = $request->hasFile('img') ? Storage::disk('public')->put('servicios',  $request->file('img')) :  $request->img_act;
        $servicio = Servicio::find($id);
        $servicio->titulo = $request->titulo;
        $servicio->descripcion = $request->descripcion;
        $servicio->img = $name;
        $servicio->save();
        return redirect()->back()->with('success', 'Se ha actualizado correctamente'); 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Servicio  $servicio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $servicio = Servicio::find($id)->delete();
        return redirect()->back()->with('success', 'Se ha eliminado correctamente'); 
    }
}
