<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Cliente;
use App\Models\Contacto;
use App\Models\ImgHome;
use App\Models\Nosotro;
use App\Models\Proyecto;
use App\Models\Servicio;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        $proyectos = Proyecto::get();
        $clientes = Cliente::get();
        $posts = Blog::get();
        $imgs = ImgHome::get();
        return view('index')->with('posts',$posts)->with('clientes',$clientes)->with('proyectos',$proyectos)->with('imgs',$imgs);
    }
    public function nosotros()
    {
        $nosotros = Nosotro::find(1);
        return view('nosotros')->with('nosotros',$nosotros);
    }

    public function servicios()
    {
        $nosotros = Servicio::get();
        return view('servicios')->with('servicios',$nosotros);
    }

    public function serviciosView($id)
    {
        $nosotros = Servicio::find($id);
        return view('servicioView')->with('servicio',$nosotros);
    }

    public function proyectos()
    {
        $nosotros = Proyecto::get();
        return view('proyectos')->with('proyectos',$nosotros);
    }

    public function proyectoView($id)
    {
        $nosotros = Proyecto::find($id);
        return view('proyectoView')->with('proyecto',$nosotros);
    }

    public function blog()
    {
        $nosotros = Blog::get();
        return view('blog')->with('posts',$nosotros);
    }

    public function blogView($id)
    {
        $nosotros = Blog::find($id);
        return view('blogView')->with('proyecto',$nosotros);
    }

    public function clientes()
    {
        $nosotros = Cliente::get();
        return view('clientes')->with('clientes',$nosotros);
    }

    public function contactStore(Request $request)
    {
        $contacto = new Contacto();
        $contacto->nombre = $request->nombre;
        $contacto->email= $request->email;
        $contacto->asunto = $request->asunto;
        $contacto->mensaje = $request->mensaje;
        $contacto->save();
        return response()->json(['status' => 'success']);

    }
}
