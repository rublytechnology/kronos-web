<?php

namespace App\Http\Controllers;

use App\Models\Proyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyectos = Proyecto::get();
        return view('admin.proyectos.index')->with('proyectos',$proyectos);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.proyectos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('img')){
            $name =  Storage::disk('public')->put('proyectos',  $request->file('img'));
        }else{
            return redirect()->back()->with('error', 'La imagen es requerida para crear el proyecto'); 
        }

        $servicio =  new Proyecto();
        $servicio->titulo = $request->titulo;
        $servicio->descripcion = $request->descripcion;
        $servicio->img = $name;
        $servicio->save();

        return redirect()->back()->with('success', 'Se ha creado correctamente'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function show(Proyecto $proyecto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicio = Proyecto::find($id);
        return view('admin.proyectos.edit')->with('proyecto',$servicio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $name = $request->hasFile('img') ? Storage::disk('public')->put('proyectos',  $request->file('img')) :  $request->img_act;
        $servicio = Proyecto::find($id);
        $servicio->titulo = $request->titulo;
        $servicio->descripcion = $request->descripcion;
        $servicio->img = $name;
        $servicio->save();
        return redirect()->back()->with('success', 'Se ha actualizado correctamente'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $servicio = Proyecto::find($id)->delete();
        return redirect()->back()->with('success', 'Se ha eliminado correctamente'); 
    }
}
