<?php

namespace App\Http\Controllers;

use App\Models\Nosotro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NosotroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nosotros= Nosotro::find(1);
        return view('admin.nosotros.index')->with('nosotro',$nosotros);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name_quienes = $request->hasFile('img_nosotros') ? Storage::disk('public')->put('nosotros',  $request->file('img_nosotros')) :  $request->img_act_nosotros;
        $name_mision = $request->hasFile('img_mision') ? Storage::disk('public')->put('nosotros',  $request->file('img_mision')) :  $request->img_act_mision;
        $name_vision = $request->hasFile('img_vision') ? Storage::disk('public')->put('nosotros',  $request->file('img_vision')) :  $request->img_act_vision;



        $home = Nosotro::updateOrCreate(
            ['id' => $request->id],
            [
                'quienes_somos' => $request->nosotros, 
                'mision' => $request->mision, 
                'vision' => $request->vision, 
                'img_vision' => $name_vision,
                'img_quines' => $name_quienes,
                'img_mision' => $name_mision,
            ]
        );

        return redirect()->back()->with('success', 'Se ha creado correctamente'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Nosotro  $nosotro
     * @return \Illuminate\Http\Response
     */
    public function show(Nosotro $nosotro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Nosotro  $nosotro
     * @return \Illuminate\Http\Response
     */
    public function edit(Nosotro $nosotro)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Nosotro  $nosotro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Nosotro $nosotro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Nosotro  $nosotro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nosotro $nosotro)
    {
        //
    }
}
