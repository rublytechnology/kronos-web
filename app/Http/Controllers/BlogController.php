<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publicaciones = Blog::get();
        return view('admin.blog.index')->with('publicaciones',$publicaciones);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('img')){
            $name =  Storage::disk('public')->put('blog',  $request->file('img'));
        }else{
            return redirect()->back()->with('error', 'La imagen es requerida para crear la publicacion'); 
        }

        if($request->hasFile('pdf')){
            $file =  Storage::disk('public')->put('blog',  $request->file('pdf'));
        }else{
            $file = null;
        }

        $servicio =  new Blog();
        $servicio->titulo = $request->titulo;
        $servicio->descripcion = $request->descripcion;
        $servicio->autor = $request->autor;
        $servicio->img = $name;
        $servicio->file = $file;
        $servicio->save();

        return redirect()->back()->with('success', 'Se ha creado correctamente'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicio = Blog::find($id);
        return view('admin.blog.edit')->with('publicacion',$servicio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->hasFile('img') ? Storage::disk('public')->put('blog',  $request->file('img')) :  $request->img_act;
        $servicio = Blog::find($id);
        $servicio->titulo = $request->titulo;
        $servicio->descripcion = $request->descripcion;
        $servicio->autor = $request->autor;
        $servicio->img = $name;
        $servicio->save();
        return redirect()->back()->with('success', 'Se ha actualizado correctamente'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $servicio = Blog::find($id)->delete();
        return redirect()->back()->with('success', 'Se ha eliminado correctamente'); 
    }
}
