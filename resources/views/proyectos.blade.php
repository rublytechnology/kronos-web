@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
@endsection
@section('content')
@include('layouts.header')


  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs bg-secondary-kronos">
        <div class="container">
          <div class="d-flex justify-content-between align-items-center">
            <h2 class="text-white">Proyectos</h2>
            <ol>
              <li><a href="/">Inicio</a></li>
              <li>Proyectos</li>
            </ol>
          </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->


   <!-- ======= Portfolio Section ======= -->
   <section id="portfolio" class="portfolio">
    <div class="container">
        <div class="section-title">
            <p class="text-kronos-primary text-center">Conoce nuestros <span class="text-kronos">proyectos</span></p>
        </div>
        <div class="row">
            <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">
                <li data-filter="*" class="filter-active">Todos los proyectos</li>
                {{-- <li data-filter=".filter-app">App</li>
                <li data-filter=".filter-card">Card</li>
                <li data-filter=".filter-web">Web</li> --}}
            </ul>
            </div>
        </div>

      <div class="row portfolio-container">
        @foreach ($proyectos as $item)
          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap" data-aos="zoom-in" 
            data-aos-easing="linear"
            data-aos-duration="1000">
              <img src="{{asset(env('STORAGE_LINK'))}}/{{$item->img}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>{{$item->titulo}}</h4>
                <p>
                    <?php
                    echo substr($item->descripcion, 0, 100) ;
                    ?>
                </p>
                <div class="portfolio-links">
                  <a href="{{asset(env('STORAGE_LINK'))}}/{{$item->img}}" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="bx bx-plus"></i></a>
                  <a href="/proyecto/{{$item->id}}" data-gall="portfolioDetailsGallery" data-vbtype="iframe" class="venobox" title="Detalle del Proyecto"><i class="bx bx-link"></i></a>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      

      </div>
  </section><!-- End Portfolio Section -->




  </main><!-- End #main -->

@include('layouts.footer')
@endsection

@section('js')

@endsection