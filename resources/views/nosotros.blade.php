@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
@endsection
@section('content')
@include('layouts.header')


  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs bg-secondary-kronos" >
        <div class="container">
          <div class="d-flex justify-content-between align-items-center">
            <h2 class="text-white">Nosotros</h2>
            <ol>
              <li><a href="/">Inicio</a></li>
              <li>Nosotros</li>
            </ol>
          </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->


    <section id="nosotros"data-aos="flip-left" data-aos-easing="linear"
      data-aos-duration="1000">
        <div class="container">
         
          <div class="row d-flex align-items-center">
            <div class="col-lg-6 pt-4 pt-lg-0">
              <div class="section-title">
                <p class="text-kronos-primary text-mobile-aling">¿ Quiénes  <span class="text-kronos">Somos </span> ?</p>
              </div>
              <?php
                echo $nosotros->quienes_somos;
              ?>
            </div>
            <div class="col-lg-6 pt-4 pt-lg-0">
                <img src="{{asset(env('STORAGE_LINK'))}}/{{$nosotros->img_quines}}" alt="img" class="img-fluid">
            </div>
          </div>

        </div>
    </section>

    <section id="mision" data-aos="flip-left" data-aos-easing="linear"
      data-aos-duration="1000">
        <div class="container">
          
          <div class="row d-flex align-items-center">
            <div class="col-lg-6 pt-4 pt-lg-0  order-sm-2 order-md-1">
              <img src="{{asset(env('STORAGE_LINK'))}}/{{$nosotros->img_mision}}" alt="img" class="img-fluid">
            </div>
              
            <div class="col-lg-6 pt-4 pt-lg-0 text-right   order-sm-1 order-md-2 text-mobile-p-aling">
                <div class="section-title">
                  <p class="text-kronos-primary text-right text-mobile-aling" > <span class="text-kronos">Misión </span> </p>
                </div>
                <?php
                    echo $nosotros->mision;
                ?>
            </div>
          </div>

        </div>
    </section>

    <section id="vision" data-aos="flip-left" data-aos-easing="linear"
      data-aos-duration="1000">
        <div class="container">
          
          <div class="row d-flex align-items-center">
            <div class="col-lg-6 pt-4 pt-lg-0">
                <div class="section-title">
                  <p class="text-kronos-primary text-mobile-aling"><span class="text-kronos">Visión </span></p>
                </div>
                <?php
                  echo $nosotros->vision;
                ?>
              </div>
              <div class="col-lg-6 pt-4 pt-lg-0">
                <img src="{{asset(env('STORAGE_LINK'))}}/{{$nosotros->img_vision}}" alt="img" class="img-fluid">
              </div>
          </div>

        </div>
    </section>




  </main><!-- End #main -->

@include('layouts.footer')
@endsection

@section('js')

@endsection