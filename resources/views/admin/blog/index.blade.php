@extends('admin.layouts.app')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('admin/plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/plugins/table/datatable/dt-global_style.css')}}">
@endsection
@section('content')

    @include('admin.layouts.sidebarDesk')
      <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="page-header">
                    <div class="page-title">
                        <h3>Administrador - Blog</h3>
                    </div>
                </div>

                <div class="row layout-top-spacing">
                    <div class="col-lg-12">
                        <div class="widget-content widget-content-area br-6">
                            <div class="row ">
                                <div class="col-12">
                                    <a href="admin-blog/create" class="btn btn-primary"> Crear Post</a>
                                </div>
                            </div>
                            <div class="table-responsive mb-4 mt-4">
                                <table id="zero-config" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Titulo</th>
                                            <th class="text-center">Imagen</th>
                                            <th class="no-content text-center">Acciones</th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                     
                                        @foreach ($publicaciones as $item)
                                        <tr>
                                            <td class="text-center"> {{$item->titulo}}</td>
                                            <td class="text-center"> <img src="{{asset(env('STORAGE_LINK'))}}/{{$item->img}}" alt="imagen" width="80">
                                            </td>
                                            <td class="text-center">
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <a type="button" href="{{route('admin-blog.edit',$item->id)}}" class="btn btn-secondary">Editar</a>
                                                    <form action="{{ route('admin-blog.destroy',$item->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                        <button type="submit" class="btn btn-secondary">Eliminar</button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    

@endsection
@section('script')

     <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('admin/plugins/table/datatable/datatables.js')}}"></script>
    <script>
        $('#zero-config').DataTable({
            "oLanguage": {
                "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
               "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [10, 20, 50],
            "pageLength": 10
        });
    </script>
     <!-- END PAGE LEVEL PLUGINS -->   
@endsection