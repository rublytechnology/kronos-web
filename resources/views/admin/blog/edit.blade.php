@extends('admin.layouts.app')
@section('css')
<link href="{{asset('admin/plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css" />   
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
@endsection
@section('content')

    @include('admin.layouts.sidebarDesk')
      <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="page-header">
                    <div class="page-title">
                        <h3>Administrador -  Edit Post</h3>
                    </div>
                </div>

                <div class="row layout-top-spacing">
                    <div class="col-lg-12">
                        <div class="widget-content widget-content-area br-6">
                            <form action=" {{route('admin-blog.update',$publicacion->id)}} "  method="POST" enctype="multipart/form-data">

                                @csrf
                                @method('PUT')

                                <div class="row">
                                    <div class="form-group mb-4 col-12">
                                        <label for="formGroupExampleInput">Titulo del Post</label>
                                        <input type="text" class="form-control" id="formGroupExampleInput" placeholder="" name="titulo" value="{{$publicacion->titulo}}" required>
                                    </div>
                                    <div class="form-group mb-4 col-12">
                                        <label for="formGroupExampleInput">Autor </label>
                                        <input type="text" class="form-control" id="formGroupExampleInput" placeholder="" name="autor" value="{{$publicacion->autor}}">
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="exampleFormControlTextarea1">Contenido</label>
                                        <textarea class="form-control"  rows="3" id="quienes" name="descripcion">{{$publicacion->descripcion}}  </textarea>
                                        <script>
                                            CKEDITOR.replace( 'quienes' ,{
                                                filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
                                                    filebrowserUploadMethod: 'form'
                                            } );
                                        </script>
                                    </div>
                                    @if(isset($publicacion))
                                    <div class="col-12 text-center m-2">
                                        <p>Imagen Actual</p> 
                                        <img src="{{asset(env('STORAGE_LINK'))}}/{{$publicacion->img}}" alt="imagen" width="400">
                                    </div>
                                    <input type="hidden" name="img_act" value="{{$publicacion->img}}">
                                    @endif
                                    <div class="custom-file-container col-12" data-upload-id="myQuienes">
                                        <label>Portada <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                        <label class="custom-file-container__custom-file" >
                                            <input type="file" class="custom-file-container__custom-file__custom-file-input" accept="image/*" name="img">
                                            <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                                            <span class="custom-file-container__custom-file__custom-file-control"></span>
                                        </label>
                                        <div class="custom-file-container__image-preview"></div>
                                    </div>
                                    <button class="btn btn-outline-primary btn-block">Guardar</button>
                                </div>
                                
                            </form>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    

@endsection
@section('script')
  
     <!-- BEGIN PAGE LEVEL PLUGINS -->
     <script src="{{asset('admin/assets/js/scrollspyNav.js')}}"></script>
     <script src="{{asset('admin/plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
 
     <script>
         //First upload
         var firstUpload = new FileUploadWithPreview('myQuienes')

     </script>
     <!-- END PAGE LEVEL PLUGINS -->   
@endsection