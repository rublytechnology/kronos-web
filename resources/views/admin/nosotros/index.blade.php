@extends('admin.layouts.app')
@section('css')
    <link href="{{asset('admin/plugins/file-upload/file-upload-with-preview.min.css')}}" rel="stylesheet" type="text/css" />   
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
@endsection
@section('content')

    @include('admin.layouts.sidebarDesk')
      <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="page-header">
                    <div class="page-title">
                        <h3>Administrador - Nosotros</h3>
                    </div>
                </div>

                <div class="row layout-top-spacing">
                    <div class="col-lg-12">
                        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                            <div class="widget-content widget-content-area br-6">

                                <form action=" {{route('admin-nosotros.store')}} "  method="POST" enctype="multipart/form-data">

                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-12">
                                            <label for="exampleFormControlTextarea1">Texto - Quienes Somos</label>
                                            <textarea class="form-control"  rows="3" id="quienes" name="nosotros"> @if(isset($nosotro)) {{$nosotro->quienes_somos}} @endif </textarea>
                                            <script>
                                                CKEDITOR.replace( 'quienes' );
                                            </script>
                                        </div>
                                        @if(isset($nosotro))
                                            <div class="col-12 text-center m-2">
                                                <p>Imagen Actual</p> 
                                                <img src="{{asset(env('STORAGE_LINK'))}}/{{$nosotro->img_quines}}" alt="imagen" width="400">
                                            </div>
                                            <input type="hidden" name="img_act_nosotros" value="{{$nosotro->img_quines}}">
                                        @endif
                                        <div class="custom-file-container col-12" data-upload-id="myQuienes">
                                            <label>Imagen - Quienes Somos <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                            <label class="custom-file-container__custom-file" >
                                                <input type="file" class="custom-file-container__custom-file__custom-file-input" accept="image/*" name="img_nosotros">
                                                <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                                                <span class="custom-file-container__custom-file__custom-file-control"></span>
                                            </label>
                                            <div class="custom-file-container__image-preview"></div>
                                        </div>

                                        <div class="form-group col-12">
                                            <label for="exampleFormControlTextarea1">Texto - Mision</label>
                                            <textarea class="form-control"  rows="3" id="mision" name="mision">@if(isset($nosotro)) {{$nosotro->mision}} @endif </textarea>
                                            <script>
                                                CKEDITOR.replace( 'mision' );
                                            </script>
                                        </div>
                                        @if(isset($nosotro))
                                            <div class="col-12 text-center m-2">
                                                <p>Imagen Actual</p> 
                                                <img src="{{asset(env('STORAGE_LINK'))}}/{{$nosotro->img_mision}}" alt="imagen" width="400">
                                            </div>
                                            <input type="hidden" name="img_act_mision" value="{{$nosotro->img_mision}}">
                                        @endif

                                        <div class="custom-file-container col-12" data-upload-id="myMision">
                                            <label>Imagen - Mision <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                            <label class="custom-file-container__custom-file" >
                                                <input type="file" class="custom-file-container__custom-file__custom-file-input" accept="image/*" name="img_mision">
                                                <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                                                <span class="custom-file-container__custom-file__custom-file-control"></span>
                                            </label>
                                            <div class="custom-file-container__image-preview"></div>
                                        </div>

                                        <div class="form-group col-12">
                                            <label for="exampleFormControlTextarea1">Texto - Vision</label>
                                            <textarea class="form-control"  rows="3" id="vision" name="vision"> @if(isset($nosotro)) {{$nosotro->vision}} @endif</textarea>
                                            <script>
                                                CKEDITOR.replace( 'vision' );
                                            </script>
                                        </div>
                                        @if(isset($nosotro))
                                            <div class="col-12 text-center m-2">
                                                <p>Imagen Actual</p> 
                                                <img src="{{asset(env('STORAGE_LINK'))}}/{{$nosotro->img_vision}}" alt="imagen" width="400">
                                            </div>
                                            <input type="hidden" name="img_act_vision" value="{{$nosotro->img_vision}}">
                                        @endif

                                        <div class="custom-file-container col-12" data-upload-id="myVision">
                                            <label>Imagen - Vision <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                            <label class="custom-file-container__custom-file" >
                                                <input type="file" class="custom-file-container__custom-file__custom-file-input" accept="image/*" name="img_vision">
                                                <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                                                <span class="custom-file-container__custom-file__custom-file-control"></span>
                                            </label>
                                            <div class="custom-file-container__image-preview"></div>
                                        </div>


                                        <input type="hidden" name="id" value="@if(isset($nosotro)) {{$nosotro->id}} @else 0 @endif">
                                        <button class="btn btn-outline-primary btn-block">Guardar</button>

                                    </div>
                                    
                                </form>

                               
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    

@endsection
@section('script')
  
     <!-- BEGIN PAGE LEVEL PLUGINS -->
     <script src="{{asset('admin/assets/js/scrollspyNav.js')}}"></script>
     <script src="{{asset('admin/plugins/file-upload/file-upload-with-preview.min.js')}}"></script>
 
     <script>
         //First upload
         var firstUpload = new FileUploadWithPreview('myQuienes')
         var firstUpload = new FileUploadWithPreview('myMision')
         var firstUpload = new FileUploadWithPreview('myVision')

     </script>
     <!-- END PAGE LEVEL PLUGINS -->   
@endsection