<!-- ======= Call To Action Section ======= -->
<section id="call-to-action" class="wow " >
    <div class="container" >
      <div class="row">
        <div class="col-lg-9 text-center text-lg-left">
          <h3 class="cta-title">Si quieres contactar con nosotros</h3>
          <p class="cta-text"> te invitamos a completar el siguiente formulario.</p>
        </div>
        <div class="col-lg-3 cta-btn-container text-center">
          <a class="cta-btn align-middle" href="{{url('contacto')}}">Contacto</a>
        </div>
      </div>

    </div>
</section><!-- End Call To Action Section -->