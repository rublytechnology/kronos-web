 <!-- ======= Footer ======= -->
 <footer id="footer" >
    <div class="footer-top ">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6">
            <div class="footer-info">
              <h3><img src="{{asset('assets/img/LOGO-BL.png')}}" alt="" width="120"> </h3>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Información</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="/servicios">Servicios</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/proyectoss">Proyectos</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/clientes">Clientes</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="/blog">Blog</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-links">
            <h4>Contacto</h4>
            <p>
              Jirón Batalla de San Juan 739, Santiago de Surco – Lima, Perú 
              <br><br>
              <strong>Teléfonos:</strong> +51 950 110 440  |  +51 989 264 629<br>
              <strong>Correo Electrónico: </strong> info@kronosconsorcio.com<br>
            </p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>


        </div>
      </div>
    </div>

  </footer><!-- End Footer -->