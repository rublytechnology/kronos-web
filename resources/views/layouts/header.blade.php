     <!-- ======= Header ======= -->
     <header id="header" class="fixed-top ">
        <div class="container d-flex align-items-center">
    
          <h1 class="logo"><a href="/"><img src="{{asset('assets/img/LOGO-BL.png')}}" alt=""></a></h1>
          <!-- Uncomment below if you prefer to use an image logo -->
          <!-- <a href="/" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
    
          <nav class="nav-menu d-none d-lg-block">
    
            <ul>
              <li class="active"><a href="/">Inicio</a></li>
              <li><a href="/nosotros">Nosotros</a></li>
              <li><a href="/servicios">Servicios</a></li>
              <li><a href="/proyectos">Proyectos</a></li>
              <li><a href="/clientes">Clientes</a></li>
              <li><a href="/blog">Blog</a></li>
              <li><a href="/contacto">Contacto</a></li>
    
            </ul>
    
          </nav><!-- .nav-menu -->
    
          <a class="ml-auto btn btn-outline-success display-button" href="https://wa.me/573126929473" target="_blank"><i class="bx bxl-whatsapp "></i> Escríbenos</a>
    
        </div>
      </header><!-- End Header -->