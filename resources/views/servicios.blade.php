@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
@endsection
@section('content')
@include('layouts.header')


  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs bg-secondary-kronos">
        <div class="container">
          <div class="d-flex justify-content-between align-items-center">
            <h2 class="text-white">Servicios</h2>
            <ol>
              <li><a href="/">Inicio</a></li>
              <li>Servicios</li>
            </ol>
          </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->


   <!-- ======= More Services Section ======= -->
   <section id="more-services" class="more-services">
    <div class="container">
        <div class="section-title">
            <p class="text-kronos-primary text-center">Conoce nuestros <span class="text-kronos">servicios</span></p>
        </div>
      <div class="row">
        @foreach ($servicios as $item)
          @php
              $img = 'storage/'.$item->img;
          @endphp
          <div class="col-md-6 d-flex align-items-stretch mt-3">
            <div class="card" style='background-image: url("{{$img}}");' data-aos="flip-left" data-aos-easing="linear"
            data-aos-duration="1000">
              <div class="card-body">
                <h5 class="card-title"><a href="">{{$item->titulo}}</a></h5>
                <p class="card-text">
                    <?php
                        echo substr($item->descripcion, 0, 100) ;
                    ?>
                </p>
                <div class="read-more"><a href="servicio/{{$item->id}}"><i class="icofont-arrow-right"></i> Ver Servicio</a></div>
              </div>
            </div>
          </div>
        @endforeach
        

      </div>

    </div>
  </section><!-- End More Services Section -->




  </main><!-- End #main -->

@include('layouts.footer')
@endsection

@section('js')

@endsection