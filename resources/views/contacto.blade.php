@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
@endsection
@section('content')
@include('layouts.header')


  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs bg-secondary-kronos">
        <div class="container">
          <div class="d-flex justify-content-between align-items-center">
            <h2 class="text-white">Contacto</h2>
            <ol>
              <li><a href="/">Inicio</a></li>
              <li>Contacto</li>
            </ol>
          </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->


    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact" data-aos="flip-left"
    data-aos-easing="ease-out-cubic"
    data-aos-duration="1000">
        <div class="container">
  
          <div>
            <iframe style="border:0; width: 100%; height: 270px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3900.858246489009!2d-76.98822198578655!3d-12.121849946600765!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c78c9806cdd7%3A0x7b013d00693eda51!2sJir%C3%B3n%20Batalla%20de%20San%20Juan%20739%2C%20Cercado%20de%20Lima%2015038%2C%20Per%C3%BA!5e0!3m2!1ses!2sve!4v1626805274593!5m2!1ses!2sve" frameborder="0" allowfullscreen></iframe>
          </div>
  
          <div class="row mt-5">
            <div class="col-lg-4">
              <div class="info">
                <div class="address">
                  <i class="icofont-google-map"></i>
                  <h4>Dirección:</h4>
                  <p>Jirón Batalla de San Juan 739, Santiago de Surco – Lima, Perú </p>
                </div>
  
                <div class="email">
                  <i class="icofont-envelope"></i>
                  <h4>Correo Electornico:</h4>
                  <p>info@kronosconsorcio.com</p>
                </div>
  
                <div class="phone">
                  <i class="icofont-phone"></i>
                  <h4>Telefono:</h4>
                  <p>+51 950 110 440 - +51 989 264 629</p>
                </div>
  
              </div>
  
            </div>
  
            <div class="col-lg-8 mt-5 mt-lg-0">
  
              <form action="{{route('contactStore')}}" method="post" role="form" class="php-email-form">
                @csrf
                <div class="form-row">
                  <div class="col-md-6 form-group">
                    <input type="text" name="nombre" class="form-control" id="name" placeholder="Nombre" data-rule="minlen:4" data-msg="Ingrese al menos 4 caracteres" />
                    <div class="validate"></div>
                  </div>
                  <div class="col-md-6 form-group">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Correo Electronico" data-rule="email" data-msg="Por favor introduzca una dirección de correo electrónico válida" />
                    <div class="validate"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="asunto" id="subject" placeholder="Asunto" data-rule="minlen:4" data-msg="Ingrese al menos 8 caracteres de asunto" />
                  <div class="validate"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="mensaje" rows="5" data-rule="required" data-msg="Por favor escribe algo para nosotros" placeholder="Mensaje"></textarea>
                  <div class="validate"></div>
                </div>
                <div class="mb-3">
                  <div class="loading">Loading</div>
                  <div class="error-message"></div>
                  <div class="sent-message">Tu mensaje ha sido enviado. ¡Gracias!</div>
                </div>
                <div class="text-center"><button type="submit">Enviar Mensaje</button></div>
              </form>
  
            </div>
  
          </div>
  
        </div>
      </section><!-- End Contact Section -->




  </main><!-- End #main -->

@include('layouts.footer')
@endsection

@section('js')

@endsection