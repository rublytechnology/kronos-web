@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
@endsection
@section('content')
@include('layouts.header')



  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs bg-secondary-kronos">
        <div class="container">
          <div class="d-flex justify-content-between align-items-center">
            <h2 class="text-white">{{$proyecto->titulo}}</h2>
          </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->

     <!-- ======= Blog Section ======= -->
     <section id="blog" class="blog " >
        <div class="container animate__animated animate__fadeInDown">
  
          <div class="row">
  
            <div class="col-12 entries">
  
              <article class="entry entry-single">
                <div class="entry-img">
                  <img src="{{asset(env('STORAGE_LINK'))}}/{{$proyecto->img}}" alt="" class="img-fluid">
                </div>
                <h2 class="entry-title">
                  <a href="#">{{$proyecto->titulo}}</a>
                </h2>
                <div class="entry-content">
                    <?php
                        echo $proyecto->descripcion ;
                    ?>
                </div>
                @if($proyecto->file)
                  <div class="mt-5">
                      <embed src="{{asset(env('STORAGE_LINK'))}}/{{$proyecto->file}}" type="application/pdf" width="100%" height="600px" />
                  </div>
                @endif
              </article><!-- End blog entry -->
  
            
  
            </div><!-- End blog entries list -->
          </div>
  
        </div>
      </section><!-- End Blog Section -->
@include('layouts.callaction')
@include('layouts.footer')
@endsection
@section('js')
@endsection