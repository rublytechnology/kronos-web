@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
@endsection
@section('content')
@include('layouts.header')


  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

      <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

      <div class="carousel-inner" role="listbox">
        @php
           $i= 1; 
        @endphp
        @foreach ($imgs as $img)
        <!-- Slide 1 -->
        <div class="carousel-item @if($i == 1) active @endif" style="background-image: url(storage/{{$img->img}})">
          <div class="carousel-container">
            <div class="container">
              <h2 class="animate__animated animate__fadeInDown">{{$img->texto}}</h2>
            </div>
          </div>
        </div>
        @php
            $i++;
        @endphp
        @endforeach

      </div>

      <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon icofont-simple-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon icofont-simple-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div>
  </section><!-- End Hero -->

  <main id="main">
    <section id="kronos" >
        <div class="container" data-aos="flip-down" data-aos-easing="ease-out-cubic"
        data-aos-duration="1000">
          <div class="section-title">
            <h2 class="animate__animated animate__fadeInDown">KRONOS</h2>
            <p class="text-white animate__animated animate__fadeInDown">Kronos <span class="text-kronos">Sí Cumple</span> </p>
          </div>
          <div class="row " >
            <div class="col-12">
              <div class="splide" id="kronos-slide">
                <div class="splide__track">
                  <ul class="splide__list">
                    @foreach ($proyectos as $item)
                      <li class="splide__slide " >
                        <div class="col-12 d-flex align-items-stretch" >
                          <article class="entry">
                            <div class="entry-img">
                              <img src="{{asset(env('STORAGE_LINK'))}}/{{$item->img}}" alt="" class="img-fluid">
                            </div>
                          </article><!-- End blog entry -->
                        </div>
                      </li>
                    @endforeach
                  </ul>
                </div>
              </div>
            </div>
          </div>

        </div>
    </section>
    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients section-bg" >
      <div class="container" data-aos="zoom-in-up" data-aos-easing="ease-out-cubic"
      data-aos-duration="1000"> 
        <div class="section-title">
          <p class="text-kronos-primary animate__animated animate__fadeInDown">Ellos <span class="text-kronos">Confiaron</span> en nosotros </p>
        </div>
        <div class="row">
          @foreach ($clientes as $item)
          <div class="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center "  >
            <img src="{{asset(env('STORAGE_LINK'))}}/{{$item->img}}" class="img-fluid" alt="">
          </div>
          @endforeach
        </div>
      </div>
    </section><!-- End Clients Section -->

    <section id="blogHome" class="blog" style="overflow-x: hidden">
      <div class="container" data-aos="zoom-out-down" data-aos-easing="ease-out-cubic"
      data-aos-duration="1000">
        <div class="section-title">
          <h2>Blog</h2>
          <p class="text-white">Publicaciones <span class="text-kronos">Recientes</span> </p>
        </div>
        <div class="row" >
          @foreach ($posts as $item)
          <div class="col-lg-4  col-md-6 d-flex align-items-stretch" >
            <article class="entry bg-white rounded" >
              <div class="entry-img" >
                <img src="{{asset(env('STORAGE_LINK'))}}/{{$item->img}}" alt="" class="img-fluid">
              </div>

              <h2 class="entry-title">
                <a href="/blog/{{$item->id}}">{{$item->titulo}}</a>
              </h2>
              <div class="entry-content">
                <p>
                    <?php
                          echo $item->descripcion ;
                    ?>
                </p>
                <div class="">
                  <a href="/blog/{{$item->id}}" class="btn btn-outline-success btn-block">Leer Mas </a>
                </div>
              </div>
            </article><!-- End blog entry -->
          </div>
          @endforeach
        </div>

      </div>
    </section>
  @include('layouts.callaction')
  @include('layouts.footer')

</main><!-- End #main -->

@endsection

@section('js')
  <script>
    document.addEventListener( 'DOMContentLoaded', function () {
      new Splide( '#kronos-slide', {
      type   : 'loop',
      perPage: 3,
        perMove: 1,
        focus      : 'center',
        trimSpace  : false,
        breakpoints: {
            600: {
                perPage: 1,
            }
        }
      }).mount();
    });
  </script>
@endsection