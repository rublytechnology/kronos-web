@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
@endsection
@section('content')
@include('layouts.header')


  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs bg-secondary-kronos">
        <div class="container">
          <div class="d-flex justify-content-between align-items-center">
            <h2 class="text-white">Blog</h2>
            <ol>
              <li><a href="/">Inicio</a></li>
              <li>Blog</li>
            </ol>
          </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
        <div class="container">
  
          <div class="row">
            @foreach ($posts as $item)
              <div class="col-lg-4  col-md-6 d-flex align-items-stretch" data-aos="fade-down"
              data-aos-easing="linear"
              data-aos-duration="1500">
                <article class="entry">
    
                  <div class="entry-img">
                    <img src="{{asset(env('STORAGE_LINK'))}}/{{$item->img}}" alt="" class="img-fluid">
                  </div>
    
                  <h2 class="entry-title">
                    <a href="/blog/{{$item->id}}">{{$item->titulo}}</a>
                  </h2>
    
                  <div class="entry-meta">
                    <ul>
                      <li class="d-flex align-items-center"><i class="icofont-user"></i> <a href="blog-single.html">{{$item->autor}}</a></li>
                      {{-- <li class="d-flex align-items-center"><i class="icofont-wall-clock"></i> <a href="blog-single.html"><time datetime="2020-01-01">Jan 1, 2020</time></a></li> --}}
                    </ul>
                  </div>
    
                  <div class="entry-content">
                   
                    <div class="">
                      <a href="/blog/{{$item->id}}" class="btn btn-outline-success btn-block">Leer publicación</a>
                    </div>
                  </div>
    
                </article><!-- End blog entry -->
              </div>
            @endforeach
  
          </div>
  
          {{-- <div class="blog-pagination" data-aos="fade-up">
            <ul class="justify-content-center">
              <li class="disabled"><i class="icofont-rounded-left"></i></li>
              <li><a href="#">1</a></li>
              <li class="active"><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#"><i class="icofont-rounded-right"></i></a></li>
            </ul>
          </div> --}}
  
        </div>
      </section><!-- End Blog Section -->

@include('layouts.footer')
@endsection

@section('js')

@endsection