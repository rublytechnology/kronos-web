@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
@endsection
@section('content')
@include('layouts.header')


  <main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs bg-secondary-kronos">
        <div class="container">
          <div class="d-flex justify-content-between align-items-center">
            <h2 class="text-white">Clientes</h2>
            <ol>
              <li><a href="/">Inicio</a></li>
              <li>Clientes</li>
            </ol>
          </div>
        </div>
    </section>
    <!-- End Breadcrumbs -->


    <section id="clientes" >
        <div class="container">
          <div class="section-title animate__animated animate__fadeInDown">
            <p class="text-kronos-primary text-mobile-aling  "> Nuestros <span class="text-kronos"> Clientes </span> </p>
          </div>
          <div class="row animate__animated animate__fadeInUp">
            @foreach ($clientes as $item)
              <div class="col-lg-2 col-md-4 col-sm-12 d-flex align-items-center justify-content-center mt-2 " >
                <img src="{{asset(env('STORAGE_LINK'))}}/{{$item->img}}" class="img-fluid img-client" alt="">
              </div>
            @endforeach
            
    
          </div>
        </div>
    </section>
      {{-- <section id="testimonials" class="testimonials ">
          <div class="container">
            <div class="section-title">
              <p class="text-kronos-primary text-mobile-aling"> sus <span class="text-kronos"> Opiniones </span> </p>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="splide" id="kronos-slide">
                  <div class="splide__track">
                    <ul class="splide__list">
                      @foreach ($clientes as $item)
                        <li class="splide__slide">
                          <div class="col-12 d-flex align-items-stretch" data-aos="fade-up" data-aos-easing="linear"
                          data-aos-duration="1000">
                            <div class="testimonial-item">
                                <img src="{{asset(env('STORAGE_LINK'))}}/{{$item->img}}" class="testimonial-img" alt="">
                                <h3>{{$item->titulo}}</h3>
                                <p>
                                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                    <?php
                                      echo $item->descripcion;
                                    ?>
                                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                </p>
                              </div>
                          </div>
                        </li>
                      @endforeach
                    
                    
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </section> --}}

    @include('layouts.callaction')

  </main><!-- End #main -->

@include('layouts.footer')
@endsection

@section('js')
  <script>
    document.addEventListener( 'DOMContentLoaded', function () {
      new Splide( '#kronos-slide', {
        type   : 'loop',
        perPage: 2,
        perMove: 1,
        focus      : 'center',
        trimSpace  : false,
        breakpoints: {
            600: {
                perPage: 1,
            }
        }
      }).mount();
    });
  </script>
@endsection